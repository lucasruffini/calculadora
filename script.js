//Variable donde se va a mostrar el resultado.
const res=document.querySelector("#res");

//Variable de donde se obtiene el primer numero.
const num1=document.querySelector("#num1");

//Variable donde se obtiene el segundo numero.
const num2=document.querySelector("#num2");

//Variable donde se donde se guarda el boton "+".
const suma=document.querySelector("#suma");

//Variable donde se donde se guarda el boton "-".
const resta=document.querySelector("#resta");

//Variable donde se donde se guarda el boton "/".
const divi=document.querySelector("#divi");

//Variable donde se donde se guarda el boton "x".
const multi=document.querySelector("#multi");

//Variable donde se donde se guarda el boton "+ cuad.".
const suma2=document.querySelector("#suma2");


//Se le agrega a cada boton su "escuchar" el evento "click" para disparar la funcion asignada.
suma.addEventListener("click", sumar); //---> function sumar()
resta.addEventListener("click", restar); //---> function restar()
multi.addEventListener("click", multiplicar); //---> funtion multiplicar()
divi.addEventListener("click", dividir); //---> function dividir()
suma2.addEventListener("click", sumarCuadrado); //---> function sumarCuadrado()


//Lo que realizan las diferentes funciones.

function sumar()
	{
		//El value de la etiqueta con id="res" tendra en su value el resultado de la suma de num1 y num2.
		res.value=Number(num1.value)+Number(num2.value);
	}

function restar()
	{
		//El value de la etiqueta con id="res" tendra en su value el resultado de la resta de num1 y num2.
		res.value=Number(num1.value)-Number(num2.value);
	}

function multiplicar()
	{
		//El value de la etiqueta con id="res" tendra en su value el resultado de la multiplicacion de num1 y num2.
		res.value=Number(num1.value)*Number(num2.value);
	}

function dividir()
	{
		//El value de la etiqueta con id="res" tendra en su value el resultado de la division de num1 y num2.
		res.value=Number(num1.value)/Number(num2.value);
	}

function sumarCuadrado()
	{
		//El value de la etiqueta con id="res" tendra en su value el resultado de la suma de los cuadrados de num1 y num2.
		res.value=Number(num1.value)**2+Number(num2.value)**2;
	}


